﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IotApplication.Services;

namespace IotApplication.Controllers
{

    /// <summary>
    /// 设备控制器
    /// </summary>
    public class DeviceController : Controller
    {


        /// <summary>
        /// 设备列表
        /// </summary>
        /// <returns></returns>
        public string List()
        {
            return CWMUtils.GetInstance().DoGet("device/list");
        }



        /// <summary>
        /// 设备展示
        /// </summary>
        /// <param name="deviceSN">设备编号</param>
        /// <returns></returns>
        public string Show(string deviceSN = "23380")
        {

            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("deviceSN", deviceSN);

            return CWMUtils.GetInstance().DoGet("device/show", param);
        }




        /// <summary>
        /// 设备展示
        /// </summary>
        /// <param name="deviceSN">设备编号</param>
        /// <returns></returns>
        public string Product(string deviceSN = "23380")
        {

            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("deviceSN", deviceSN);

            return CWMUtils.GetInstance().DoGet("device/product", param);
        }

    }
}
