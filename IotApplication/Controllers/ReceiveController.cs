﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace IotApplication.Controllers
{

    /// <summary>
    /// 消息订阅
    /// </summary>
    public class ReceiveController : Controller
    {
        //
        // GET: /Receive/
        [System.Web.Mvc.HttpGet]
        public string Index()
        {
            return "接收GET数据";
        }


        // GET: /Receive/
        [System.Web.Mvc.HttpPost]
        public string Index(FormCollection collection)
        {
            collection.GetValue("clazz");

            //Request.QueryString.GetKey("");

            return "接收POST数据";
        }

        public string Json([FromBody] string jsonStr)
        {
            return "接收json" + jsonStr;
        }


    }
}
