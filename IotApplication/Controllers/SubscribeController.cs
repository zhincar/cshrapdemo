﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IotApplication.Services;

namespace IotApplication.Controllers
{

    /// <summary>
    /// 订阅控制器
    /// </summary>
    public class SubscribeController : Controller
    {

        /// <summary>
        /// 消息订阅列表
        /// </summary>
        /// <returns></returns>
        public String List()
        {
            return CWMUtils.GetInstance().DoGet("subscribe/list");
        }

        /// <summary>
        /// 消息订阅申请
        /// </summary>
        /// <returns></returns>
        public string Send()
        {

            Dictionary<String, Object> param = new Dictionary<String, Object>();
            param.Add("title", "测试消息订阅");
            param.Add("productId", 1);
            param.Add("protocol", "0XB0");
            param.Add("httpUrl", "http://192.168.1.188:8026/receive");

            return CWMUtils.GetInstance().DoGet("subscribe/send", param);
        }

        /// <summary>
        /// 取消消息订阅
        /// </summary>
        /// <returns></returns>
        public string Cannel()
        {

            Dictionary<String, Object> param = new Dictionary<String, Object>();
            param.Add("productId", 1);
            param.Add("protocol", "0XB0");

            return CWMUtils.GetInstance().DoGet("subscribe/cancel", param);
        }



        /// <summary>
        /// 测试消息订阅
        /// </summary>
        /// <returns></returns>
        public string Test()
        {
            Dictionary<String, Object> param = new Dictionary<String, Object>();
            param.Add("deviceSN", "23380");
            param.Add("protocol", "0XB0");

            return CWMUtils.GetInstance().DoGet("subscribe/test", param);
        }

    }
}
