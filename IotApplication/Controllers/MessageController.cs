﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IotApplication.Services;
using Newtonsoft.Json;

namespace IotApplication.Controllers
{

    /// <summary>
    ///  消息控制器
    /// </summary>
    public class MessageController : Controller
    {


        /// <summary>
        /// 消息列表
        /// </summary>
        /// <param name="deviceSN">设备编号</param>
        /// <returns></returns>
        public string List(string deviceSN = "23380")
        {


            Dictionary<string, object> param = new Dictionary<string, object>();

            param.Add("deviceSN", deviceSN);

            return CWMUtils.GetInstance().DoGet("message/list", param);
        }

        /// <summary>
        /// 消息详情
        /// </summary>
        /// <param name="msgId">消息编号</param>
        /// <returns></returns>
        public string Show(string msgId = "319502")
        {

            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add("msgId", msgId);



            string json = CWMUtils.GetInstance().DoGet("message/show", param);

            object obj = Newtonsoft.Json.JsonConvert.DeserializeObject(json);
            

            return obj.ToString();

        }

    }
}
