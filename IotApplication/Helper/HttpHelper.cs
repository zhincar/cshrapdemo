﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace IotApplication.Helper
{
    public class HttpHelper
    {




        #region get/post原始方法


        /// <summary>
        /// 发送http post请求
        /// </summary>
        /// <param name="url">地址</param>
        /// <param name="parameters">查询参数集合</param>
        /// <param name="headers">头</param>
        /// <returns></returns>
        public static HttpWebResponse DoPostHttpResponse(string url, IDictionary<string, string> parameters, IDictionary<String, String> headers)
        {
            HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;//创建请求对象

            if (request == null)
            {
                return null;
            }

            request.Method = "POST";//请求方式
            request.ContentType = "application/x-www-form-urlencoded";//链接类型

            if (!(headers == null || headers.Count == 0))
            {

                foreach (string key in headers.Keys)
                {
                    request.Headers[key] = headers[key];
                }

            }

            //构造查询字符串
            if (!(parameters == null || parameters.Count == 0))
            {
                StringBuilder buffer = new StringBuilder();
                bool first = true;
                foreach (string key in parameters.Keys)
                {

                    if (!first)
                    {
                        buffer.AppendFormat("&{0}={1}", key, parameters[key]);
                    }
                    else
                    {
                        buffer.AppendFormat("{0}={1}", key, parameters[key]);
                        first = false;
                    }
                }


                byte[] data = Encoding.UTF8.GetBytes(buffer.ToString());
                //写入请求流
                using (Stream stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }


            }

            return request.GetResponse() as HttpWebResponse;
        }





        /// <summary>
        ///发送 GET 请求（HTTP），带输入数据
        /// </summary>
        /// <param name="url">地址</param>
        /// <param name="parameters">查询参数集合</param>
        /// <param name="headers">头</param>
        /// <returns></returns>
        public static HttpWebResponse DoGetHttpResponse(String url, IDictionary<String, Object> parameters, IDictionary<String, String> headers)
        {
            
            if (parameters != null && parameters.Count >= 1)
            {

                StringBuilder buffer = new StringBuilder();

                bool first = true;

                foreach (string key in parameters.Keys)
                {
                    if (!first)
                    {
                        buffer.AppendFormat("&{0}={1}", key, parameters[key]);
                    }
                    else
                    {
                        buffer.AppendFormat("?{0}={1}", key, parameters[key]);
                        first = false;
                    }
                }


                url += buffer.ToString();
            }



            return DoGetHttpResponse(url, headers);

        }




        /// <summary>
        /// 发送http Get请求
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static HttpWebResponse DoGetHttpResponse(String url)
        {
            return DoGetHttpResponse(url, new Dictionary<String, Object>(), new Dictionary<String, String>());
        }


        /// <summary>
        /// 发送http Get请求
        /// </summary>
        /// <param name="url"></param>
        /// <param name="headers"></param>
        /// <returns></returns>
        public static HttpWebResponse DoGetHttpResponse(string url, IDictionary<String, String> headers)
        {
            HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;

            if (request == null)
            {
                return null;
            }


            request.Method = "GET";
            request.ContentType = "application/x-www-form-urlencoded";//链接类型
            if (!(headers == null || headers.Count == 0))
            {

                foreach (string key in headers.Keys)
                {
                    request.Headers[key] = headers[key];
                }

            }

            return request.GetResponse() as HttpWebResponse;

        }


        /// <summary>
        /// 从HttpWebResponse对象中提取响应的数据转换为字符串
        /// </summary>
        /// <param name="webresponse"></param>
        /// <returns></returns>
        public static string GetResponseString(HttpWebResponse webresponse)
        {
            using (Stream s = webresponse.GetResponseStream())
            {
                if (s == null)
                {
                    return String.Empty;
                }

                StreamReader reader = new StreamReader(s, Encoding.UTF8);
                return reader.ReadToEnd();
            }
        }

        #endregion


    }





}