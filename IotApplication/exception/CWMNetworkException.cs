﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace IotApplication.exception
{
    public class CWMNetworkException : CWMException
    {
      
        public CWMNetworkException() { }

        public CWMNetworkException(string message) : base(message) { }

        public CWMNetworkException(string message, Exception inner) : base(message, inner) { }

        public CWMNetworkException(SerializationInfo info, StreamingContext context) : base(info, context) { }

     

    }
}