﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace IotApplication
{
    public class CWMException : ApplicationException
    {


        public CWMException() { }

        public CWMException(string message) : base(message) { }

        public CWMException(string message, Exception inner) : base(message, inner) { }

        public CWMException(SerializationInfo info, StreamingContext context) : base(info, context) { }



    }
}