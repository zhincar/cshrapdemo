﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using IotApplication.exception;
using IotApplication.Helper;
using Microsoft.Ajax.Utilities;

namespace IotApplication.Services
{

    /// <summary>
    /// 工具类
    /// </summary>
    public class CWMUtils
    {

        private static CWMUtils _cwmutils = null;

        private static readonly object Locker = new object();

        /// <summary>
        /// apikey
        /// </summary>
        private readonly string _apiKey;

        /// <summary>
        /// apisecret
        /// </summary>
        private readonly string _apiSecret;

        /// <summary>
        /// httpurl
        /// </summary>
        private readonly string _httpUrl;

        private CWMUtils()
        {
            _apiKey = System.Configuration.ConfigurationManager.AppSettings["ApiKey"];
            _apiSecret = System.Configuration.ConfigurationManager.AppSettings["ApiSecret"];
            _httpUrl = System.Configuration.ConfigurationManager.AppSettings["HttpUrl"];


        }

        /// <summary>
        /// 单例
        /// </summary>
        /// <returns></returns>
        public static CWMUtils GetInstance()
        {
            lock (Locker)
            {
                if (_cwmutils == null)
                {
                    _cwmutils = new CWMUtils();
                }

                return _cwmutils;
            }
        }


        /// <summary>
        /// get
        /// </summary>
        /// <param name="methodUrl">地址</param>
        /// <returns></returns>
        public String DoGet(String methodUrl)
        {
            return DoGet(methodUrl, null);
        }


        /// <summary>
        /// get
        /// </summary>
        /// <param name="methodUrl">地址</param>
        /// <param name="parameters">参数</param>
        /// <returns></returns>
        public String DoGet(String methodUrl, Dictionary<String, Object> parameters)
        {

            String httpUrl = GetApiUrl(methodUrl);

            HttpWebResponse response = HttpHelper.DoGetHttpResponse(httpUrl, parameters, GetApiHeader());


            if (response.StatusCode == HttpStatusCode.OK)
            {

                return HttpHelper.GetResponseString(response);

            }

            throw new CWMNetworkException("网络异常");


        }

        /// <summary>
        /// 拼装API URL
        /// </summary>
        /// <param name="methodUrl">地址</param>
        /// <returns></returns>
        private String GetApiUrl(String methodUrl)
        {
            return _httpUrl + methodUrl;
        }

        /// <summary>
        /// 拼装头文件
        /// </summary>
        /// <returns></returns>
        private IDictionary<String, String> GetApiHeader()
        {

            IDictionary<String, String> headers = new Dictionary<string, string>();

            headers.Add("X-CWMAPI-ApiKey", _apiKey);
            headers.Add("X-CWMAPI-ApiSecret", _apiSecret);

            return headers;
        }







    }
}